#r "packages/Suave/lib/net40/Suave.dll"
let (^) l r = sprintf "%s%s" l r
let mutable userKey1 = ref ""
let mutable userKey2 = ref ""   

open Suave
open Suave.Web
open Suave.Filters
open Suave.Successful
open Suave.Operators
open Suave.Cookie
   

let app = 
    choose [
        path "/" >=> OK "Bonjour"
        path "/test" >=> OK "lol"
    ]